from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import os.path


class Lz4Conan(ConanFile):
    name = "lz4"
    version = "1.7.5"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources="*.zip"

    def source(self):
        archive_file = "lz4-1.7.5.zip"
        tools.unzip(archive_file)

    def configure(self):
        pass

    def build(self):
        
        if self.settings.os=='Windows' and self.settings.compiler == "Visual Studio":
            sln_file = os.path.join("lz4-1.7.5","visual","VS2010","lz4.sln")
            build_command = tools.build_sln_command(self.settings,sln_file,targets=["liblz4-dll"])
            command = "%s && %s" %(tools.vcvars_command(self.settings),build_command)
            self.run(command)
        elif self.settings.os == 'Linux':
            env = {"PREFIX":self.package_folder,}
            if self.options.shared:
                env["BUILD_STATIC"] = "no"
            else:
                env["BUILD_STATIC"] = "yes"

            if self.settings.build_type=="Debug":
                env["CFLAGS"]="-O0"
                  
            with tools.environment_append(env):
                self.run('cd lz4-1.7.5 && make')


    def package(self):
        self.copy("*.h", dst="include",
                src=os.path.join(self.source_folder,"lz4-1.7.5","lib"))
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.so.*", dst='lib', keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lz4"]
