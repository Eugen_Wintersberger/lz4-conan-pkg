#include <iostream>
extern "C"{
#include <lz4.h>
}

int main() {
    char source[1024];
    char dest[1024];
    std::cout<<LZ4_versionNumber()<<std::endl;
    std::cout<<LZ4_versionString()<<std::endl;

    LZ4_compress_default(source,dest,1024,1024);
    //LZ4_decompress_fast(source,dest,1024);

    return 0;
}
